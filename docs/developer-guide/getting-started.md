## Setting up your development environment

1. Make sure [poetry](https://python-poetry.org/docs/#installation) is installed.
2. Run: `poetry config virtualenvs.in-project true`
3. Ensure you are using supported python version (3.10 and above): `poetry env use 3.10`
4. Run: `poetry install`

## Running tests

There are two separate test suites, for unit tests, and for integration tests.
After they're run, the test coverage will be output to the console.

### Unit tests

To run unit test suite, run `python run_unit_tests.py`.

### Integration tests

To run integration test suite, first make sure you are [connected](https://insta360.whitebox.aero/#connecting-to-the-wifi)
to the camera's Wi-Fi network.

Then, run `python run_integration_tests.py`.

## Contribution Guidelines

1. Write tests for each new feature: integration and unit tests with 90%+ coverage.
2. [Google style docstrings](https://mkdocstrings.github.io/griffe/docstrings/#google-style)
   should be used.

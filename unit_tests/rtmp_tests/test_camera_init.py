import logging
import unittest
import asyncio.log

from insta360.rtmp import Client
from unit_tests.common import ensure_camera_connected, override_logging_level
from unittest.mock import patch


class TestCameraOpen(unittest.TestCase):
    def test_camera_init_custom_logger(self):
        logger = logging.getLogger("insta360")
        with override_logging_level(asyncio.log.logger, logging.INFO):
            camera = Client(logger=logger)
        assert camera.logger is logger

    @patch(
        "insta360.rtmp.rtmp.ensure_camera_connected",
        side_effect=ensure_camera_connected,
    )
    @override_logging_level(asyncio.log.logger, logging.INFO)
    def test_ensure_camera_connected(self, mock_ensure_camera_connected):
        # WHEN client is initialized without `verify_camera_connected`, or
        #      specified as False
        camera1 = Client()
        camera2 = Client(verify_camera_connected=False)

        # THEN camera connectivity should not be checked
        mock_ensure_camera_connected.assert_not_called()

        # further,
        # WHEN client is initialized with `verify_camera_connected`
        camera3 = Client(verify_camera_connected=True)

        # THEN camera connectivity should be checked
        mock_ensure_camera_connected.assert_called_once()

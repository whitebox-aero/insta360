import unittest

from insta360.osc import *


class TestModel(unittest.TestCase):
    def test_list_file_type(self):
        self.assertEqual(ListFileType.ALL.value, "all")
        self.assertEqual(ListFileType.IMAGE.value, "image")
        self.assertEqual(ListFileType.VIDEO.value, "video")

    def test_thumbnail_model(self):
        thumbnail_data = {"thumbnail": base64.b64encode(b"test_thumbnail").decode()}
        thumbnail = Thumbnail(**thumbnail_data)
        self.assertEqual(thumbnail.base64, thumbnail_data["thumbnail"])
        self.assertEqual(thumbnail.content, b"test_thumbnail")

        # Test with empty base64
        thumbnail_data = {"thumbnail": ""}
        thumbnail = Thumbnail(**thumbnail_data)
        self.assertIsNone(thumbnail.base64)
        self.assertIsNone(thumbnail.content)

    def test_entry_model(self):
        entry_data = {
            "name": "test.jpg",
            "fileUrl": "http://example.com/test.jpg",
            "size": 1024,
            "width": 1920,
            "height": 1080,
            "dateTimeZone": "UTC",
            "isProcessed": True,
            "previewUrl": "http://example.com/preview.jpg",
            "_localFileUrl": "/local/path/test.jpg",
            "_dateTime": "2023-01-01T00:00:00Z",
            "_thumbnailSize": 256,
            "thumbnail": base64.b64encode(b"test_thumbnail").decode(),
        }
        entry = Entry(**entry_data)
        self.assertEqual(entry.name, entry_data["name"])
        self.assertEqual(entry.fileUrl, entry_data["fileUrl"])
        self.assertEqual(entry.size, entry_data["size"])
        self.assertEqual(entry.width, entry_data["width"])
        self.assertEqual(entry.height, entry_data["height"])
        self.assertEqual(entry.dateTimeZone, entry_data["dateTimeZone"])
        self.assertEqual(entry.isProcessed, entry_data["isProcessed"])
        self.assertEqual(entry.previewUrl, entry_data["previewUrl"])
        self.assertEqual(entry.localFileUrl, entry_data["_localFileUrl"])
        self.assertEqual(entry.dateTime, entry_data["_dateTime"])
        self.assertEqual(entry.thumbnailSize, entry_data["_thumbnailSize"])
        self.assertEqual(entry.thumbnail.base64, entry_data["thumbnail"])
        self.assertEqual(entry.thumbnail.content, b"test_thumbnail")

    def test_list_files_results_model(self):
        entry_data = {
            "name": "test.jpg",
            "fileUrl": "http://example.com/test.jpg",
            "size": 1024,
            "width": 1920,
            "height": 1080,
            "dateTimeZone": "UTC",
            "isProcessed": True,
            "previewUrl": "http://example.com/preview.jpg",
            "_localFileUrl": "/local/path/test.jpg",
            "_dateTime": "2023-01-01T00:00:00Z",
            "_thumbnailSize": 256,
            "thumbnail": base64.b64encode(b"test_thumbnail").decode(),
        }
        list_files_results = ListFilesResults(
            entries=[Entry(**entry_data)], totalEntries=1
        )
        self.assertEqual(len(list_files_results.entries), 1)
        self.assertEqual(list_files_results.totalEntries, 1)

    def test_delete_files_results_model(self):
        delete_files_results = DeleteFilesResults(
            fileUrls=["http://example.com/test.jpg"]
        )
        self.assertEqual(len(delete_files_results.fileUrls), 1)
        self.assertEqual(
            delete_files_results.fileUrls[0], "http://example.com/test.jpg"
        )

    def test_osc_response_model(self):
        osc_response = OSCResponse(name="test_response", state="done")
        self.assertEqual(osc_response.name, "test_response")
        self.assertEqual(osc_response.state, "done")

    def test_error_model(self):
        error = Error(code="404", message="Not Found")
        self.assertEqual(error.code, "404")
        self.assertEqual(error.message, "Not Found")

    def test_error_response_model(self):
        error = Error(code="404", message="Not Found")
        error_response = ErrorResponse(error=error)
        self.assertEqual(error_response.error.code, "404")
        self.assertEqual(error_response.error.message, "Not Found")

    def test_list_files_response_model(self):
        entry_data = {
            "name": "test.jpg",
            "fileUrl": "http://example.com/test.jpg",
            "size": 1024,
            "width": 1920,
            "height": 1080,
            "dateTimeZone": "UTC",
            "isProcessed": True,
            "previewUrl": "http://example.com/preview.jpg",
            "_localFileUrl": "/local/path/test.jpg",
            "_dateTime": "2023-01-01T00:00:00Z",
            "_thumbnailSize": 256,
            "thumbnail": base64.b64encode(b"test_thumbnail").decode(),
        }
        list_files_results = ListFilesResults(
            entries=[Entry(**entry_data)], totalEntries=1
        )
        list_files_response = ListFilesResponse(
            name="list_files", state="done", results=list_files_results
        )
        self.assertEqual(list_files_response.name, "list_files")
        self.assertEqual(list_files_response.state, "done")
        self.assertEqual(len(list_files_response.results.entries), 1)
        self.assertEqual(list_files_response.results.totalEntries, 1)

    def test_delete_files_response_model(self):
        delete_files_results = DeleteFilesResults(
            fileUrls=["http://example.com/test.jpg"]
        )
        delete_files_response = DeleteFilesResponse(
            name="delete_files", state="done", results=delete_files_results
        )
        self.assertEqual(delete_files_response.name, "delete_files")
        self.assertEqual(delete_files_response.state, "done")
        self.assertEqual(len(delete_files_response.results.fileUrls), 1)
        self.assertEqual(
            delete_files_response.results.fileUrls[0], "http://example.com/test.jpg"
        )

import asyncio
import time
import unittest
from copy import deepcopy
from unittest.mock import (
    patch,
    Mock,
    ANY,
)

from insta360.rtmp import Client, EventManager, CameraNotConnectedException
from unit_tests.common import ensure_camera_connected, MockResponse


class TestEvents(unittest.TestCase):
    def setUp(self):
        with (
            patch("insta360.rtmp.rtmp.Client._init_rcv_thread"),
            patch("insta360.rtmp.rtmp.ensure_camera_connected"),
        ):
            self.camera = Client()

        self.event_manager = self.camera._event_manager
        return super().setUp()

    @patch("insta360.rtmp.events.EventManager.register_handler")
    def test_handler_registering_without_wait(self, mock_register_handler):
        # GIVEN a callback function that needs to be called when a certain event
        #       is triggered
        event_name = "test_event"
        handler_uid = "heya"

        # WHEN it is registered on a client
        @self.camera.on_event(event_name=event_name, uid=handler_uid)
        def event_handler(**kwargs):
            pass

        # THEN the event should be registered in the event manager
        mock_register_handler.assert_called_once_with(
            event_name,
            event_handler,
            ANY,  # handler UID
            False,  # handler `wait` flag
        )

    @patch("insta360.rtmp.events.EventManager.register_handler")
    def test_handler_registering_with_wait(self, mock_register_handler):
        # GIVEN a callback function that needs to be called when a certain event
        #       is triggered
        event_name = "test_event"
        handler_uid = "heya"

        # WHEN it is registered on a client
        @self.camera.on_event(event_name=event_name, uid=handler_uid, wait=True)
        def event_handler(**kwargs):
            pass

        # THEN the event should be registered in the event manager
        mock_register_handler.assert_called_once_with(
            event_name,
            event_handler,
            ANY,  # handler UID
            True,  # handler `wait` flag
        )

    # region specific event testing

    def _setup_event(self, register_fn, call_sentinel):
        @register_fn()
        async def event_handler(**kwargs):
            call_sentinel(**kwargs)

    def _assert_event_called(self, call_sentinel, call_kwargs):
        time.sleep(1)  # Wait for the event loop to process the called handler
        call_sentinel.assert_called_once_with(**call_kwargs)

    @patch("socket.socket", return_value=Mock())
    @patch("select.poll", return_value=Mock())
    @patch("insta360.rtmp.rtmp.ensure_camera_connected")
    @patch("insta360.rtmp.Client.KeepAliveTimer")
    @patch("insta360.rtmp.Client._send_message")
    @patch("insta360.rtmp.Client._send_packet")
    def test_event_connect(self, *_mocks):
        # GIVEN an `on_connect` event is set up
        call_sentinel = Mock()
        self._setup_event(self.camera.on_connect, call_sentinel)

        # WHEN camera is connected
        self.camera.open()

        # THEN the event should be fired with the call kwargs
        call_kwargs = {
            "client": self.camera,
        }
        self._assert_event_called(call_sentinel, call_kwargs)

    def test_event_disconnect(self):
        # GIVEN an `on_disconnect` event is set up
        call_sentinel = Mock()
        self._setup_event(self.camera.on_disconnect, call_sentinel)

        # WHEN camera is disconnected
        self.camera.close()

        # THEN the event should be fired with the call kwargs
        call_kwargs = {
            "client": self.camera,
        }
        self._assert_event_called(call_sentinel, call_kwargs)

    def test_event_video_stream(self):
        # GIVEN an `on_video_stream` event is set up
        call_sentinel = Mock()
        self._setup_event(self.camera.on_video_stream, call_sentinel)

        content = b"SENDING-VIDEO-DATA-TRANSMISSION-FIVER-NINER-OVER"
        packet_data = b"".join(
            [
                b"\x01\x00\x00",  # response type for the video transmission
                b"\x00" * 9,  # the rest of the header for the video tx is empty
                content,
            ]
        )

        # WHEN camera has received some transmission data
        self.camera._parse_packet(packet_data)

        # THEN the event should be fired with the call kwargs
        call_kwargs = {
            "client": self.camera,
            "content": content,
        }
        self._assert_event_called(call_sentinel, call_kwargs)

    def test_event_error_no_connect(self):
        # GIVEN an `on_disconnect` event is set up
        call_sentinel = Mock()
        self._setup_event(self.camera.on_error, call_sentinel)
        raised_exception = CameraNotConnectedException()

        # WHEN camera is trying to connect but it cannot connect
        with patch(
            "insta360.rtmp.rtmp.ensure_camera_connected",
            side_effect=raised_exception,
        ):
            with self.assertRaises(CameraNotConnectedException):
                self.camera.open()

        # THEN the event should be fired with the call kwargs
        call_kwargs = {
            "client": self.camera,
            "message": "This device is not connected to the camera wifi",
            "exception": raised_exception,
        }
        self._assert_event_called(call_sentinel, call_kwargs)

    # endregion specific event testing


class TestEventManager(unittest.TestCase):
    def setUp(self):
        self.manager = EventManager()
        return super().setUp()

    @patch("insta360.rtmp.events.EventManager._ensure_started")
    def test_register_handler(self, _mock_ensure_started):
        # GIVEN a handler that takes an event
        event_name_1 = "another_event"
        handler_1 = lambda **kwargs: None

        event_name_2 = "more_events"
        handler_2 = lambda **kwargs: None
        uid_2 = "custom_uid"

        wait = False

        # WHEN it is registered without `wait`
        self.manager.register_handler(event_name_1, handler_1)
        self.manager.register_handler(event_name_2, handler_2, uid_2)

        # THEN it should exist in the registry
        assert len(self.manager._registry) == 2, "Handler count does not match!"
        assert event_name_1 in self.manager._registry
        assert event_name_2 in self.manager._registry

        event_1_registry = self.manager._registry[event_name_1]
        reg_e1_uid, reg_e1_handler = next(iter(event_1_registry.items()))
        assert reg_e1_handler == (handler_1, wait)
        assert reg_e1_uid.startswith("{}-".format(handler_1.__name__))

        event_2_registry = self.manager._registry[event_name_2]
        reg_e2_uid, reg_e2_handler = next(iter(event_2_registry.items()))
        assert reg_e2_handler == (handler_2, wait)
        assert reg_e2_uid == uid_2

    @patch("insta360.rtmp.events.EventManager._ensure_started")
    def test_register_handler_with_wait(self, _mock_ensure_started):
        # GIVEN a handler that takes an event
        event_name_1 = "another_event"
        handler_1 = lambda **kwargs: None

        event_name_2 = "more_events"
        handler_2 = lambda **kwargs: None
        uid_2 = "custom_uid"

        wait = True

        # WHEN it is registered with `wait`
        self.manager.register_handler(event_name_1, handler_1, wait=wait)

        # THEN it should exist in the registry
        assert len(self.manager._registry) == 1, "Handler count does not match!"
        assert event_name_1 in self.manager._registry

        event_1_registry = self.manager._registry[event_name_1]
        reg_e1_uid, reg_e1_handler = next(iter(event_1_registry.items()))
        assert reg_e1_handler == (handler_1, wait)
        assert reg_e1_uid.startswith("{}-".format(handler_1.__name__))

    @patch("insta360.rtmp.events.EventManager._ensure_started")
    def test_unregister_handler(self, _mock_ensure_started):
        # GIVEN a handler has been registered already with a specific UID
        event_name = "happening"
        handler = lambda **kwargs: None
        uid = "now-you-see-me-now-you-dont"

        self.manager.register_handler(event_name, handler, uid)
        assert uid in self.manager._registry[event_name]

        # WHEN the handler is unregistered
        self.manager.unregister_handler(event_name, uid)

        # THEN the handler does not exist anymore in the registry
        assert uid not in self.manager._registry[event_name]

    @patch("threading.Thread.start")
    def test_event_loop_started_when_event_registered(
        self,
        mock_thread_start,
    ):
        # GIVEN that there is an event that will be registered
        mock_thread_start.assert_not_called()

        # WHEN registered
        self.manager.register_handler(
            "foo",
            lambda **kwargs: None,
        )

        # THEN the event loop thread should be started
        mock_thread_start.assert_called_once()

        # further,
        # WHEN more events are registered afterwards
        self.manager.register_handler(
            "bar",
            lambda **kwargs: None,
        )
        self.manager.register_handler(
            "baz",
            lambda **kwargs: None,
        )

        # THEN the event loop thread should have already been started and
        #      further registrations should not trigger it again
        mock_thread_start.assert_called_once()

    @patch("threading.Thread.start")
    @patch("asyncio.run_coroutine_threadsafe")
    def test_handlers_called_with_passed_kwargs(
        self,
        mock_run_coroutine_threadsafe,
        mock_thread_start,
    ):
        # GIVEN that there are entries in the manager's registry
        event_name = "yet another one!"
        call_kwargs = {"hello": 1, "world": 2}
        call_sentinel = Mock()

        async def handler(**kwargs):
            call_sentinel(**kwargs)

        self.manager.register_handler(event_name, handler)

        # WHEN the event is triggered
        self.manager.process_event(event_name, **call_kwargs)

        # AND the event is actually processed
        #   To avoid launching a thread in this test, we'll be creating a new
        #   event loop and running it locally
        local_event_loop = asyncio.new_event_loop()
        coro = mock_run_coroutine_threadsafe.call_args.args[0]
        local_event_loop.run_until_complete(coro)

        # THEN the handler should be called with all the call kwargs
        call_sentinel.assert_called_once_with(**call_kwargs)

    def test_handlers_called_with_passed_kwargs_with_wait(self):
        # GIVEN that there are entries in the manager's registry
        event_name = "yet another one!"
        call_kwargs = {"hello": 1, "world": 2}

        zero_ts: float = None
        first_ts: float = None
        second_ts: float = None

        async def handler_0(**kwargs):
            nonlocal zero_ts

            await asyncio.sleep(3)
            zero_ts = time.time()

        async def handler_1(**kwargs):
            nonlocal first_ts

            await asyncio.sleep(3)
            first_ts = time.time()

        async def handler_2(**kwargs):
            nonlocal second_ts

            await asyncio.sleep(3)
            second_ts = time.time()

        self.manager.register_handler(event_name, handler_0)
        self.manager.register_handler(event_name, handler_1, wait=True)
        self.manager.register_handler(event_name, handler_2, wait=True)

        # WHEN the event is triggered
        start_ts = time.time()
        self.manager.process_event(event_name, **call_kwargs)

        # THEN the handlers should be called in consecutive order, with the
        #      second handler being called after the first one finishes
        assert zero_ts is not None, "Non-wait handler not called"
        assert first_ts is not None, "First handler not called"
        assert second_ts is not None, "Second handler not called"

        first_delta = first_ts - start_ts
        second_delta = second_ts - first_ts
        zeroth_delta = abs(zero_ts - first_ts)

        # Zeroth delta needs to be less than 1 second because it should be
        # called before the first handler asynchronously, so their recorded
        # times should be close to each other
        assert zeroth_delta < 1, "Zeroth handler not called asynchronously"

        assert first_delta >= 3, "First handler called too early"
        assert second_delta >= 3, "Second handler called too early"

    @patch("threading.Thread.start")
    def test_event_thread_started_once(self, mock_thread_start):
        # GIVEN there are some events and handlers (more than 1)
        event_name_1 = "any_event"

        async def any_handler(**kwargs):
            pass

        event_name_2 = "any_other_event"

        async def any_other_handler(**kwargs):
            pass

        # WHEN they are registered to the event manager
        self.manager.register_handler(event_name_1, any_handler)
        self.manager.register_handler(event_name_2, any_other_handler)

        # THEN both events should be registered, but the event loop thread
        #      should have only been started exactly once
        mock_thread_start.assert_called_once()
        assert len(self.manager._registry[event_name_1]) == 1
        assert len(self.manager._registry[event_name_2]) == 1

    def test_event_handler_malformed(self):
        event_name = "good_event"

        async def bad_handler(does_not_have_all_args):
            pass

        self.manager.register_handler(event_name, bad_handler)

        with patch.object(self.manager.logger, "exception") as logger_exc:
            self.manager.process_event(event_name, something_else=True)

        logger_exc.assert_called_once()

    def test_waited_event_handler_failing_not_raising_exception(self):
        event_name = "failing_event"

        async def failing_handler(**kwargs):
            raise Exception("Execute order 66")

        self.manager.register_handler(event_name, failing_handler, wait=True)

        with patch.object(self.manager.logger, "exception") as logger_exc:
            self.manager.process_event(event_name)

        logger_exc.assert_called_once_with(
            "Error occurred while waiting for handler to finish "
            '("{}") for event "{}"'.format(
                failing_handler.__name__,
                event_name,
            ),
        )

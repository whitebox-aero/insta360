import unittest
import argparse

from unittest.mock import patch, MagicMock
from tools.insta360_cmd import handle_camera_commands


class TestInsta360CMD(unittest.TestCase):
    @patch("tools.insta360_cmd.Client")
    @patch("tools.insta360_cmd.logger")
    def test_handle_camera_commands_preview(self, mock_logger, mock_client):
        mock_client_instance = MagicMock()
        mock_client.return_value = mock_client_instance

        args = argparse.Namespace(
            command=["preview"],
            output=None,
            display=False,
            no_hwaccel=False,
        )

        handle_camera_commands(args)

        mock_client.assert_called_once_with(
            host="192.168.42.1",
            port=6666,
            enable_hwaccel=True,
        )
        mock_client_instance.open.assert_called_once()
        mock_client_instance.start_preview_stream.assert_called_once()
        mock_client_instance.start_capture.assert_not_called()
        mock_client_instance.on_video_stream.assert_not_called()

        mock_logger.info.assert_any_call("Connecting to camera")
        mock_logger.info.assert_any_call("Starting preview stream")
        mock_logger.info.assert_any_call("Commands executed successfully")
        mock_logger.info.assert_any_call(
            "Press Ctrl+C to stop the commands and close the connection"
        )

    @patch("tools.insta360_cmd.Client")
    @patch("tools.insta360_cmd.logger")
    def test_handle_camera_commands_capture(self, mock_logger, mock_client):
        mock_client_instance = MagicMock()
        mock_client.return_value = mock_client_instance

        args = argparse.Namespace(
            command=["capture"],
            output="./output/dump.mp4",
            display=False,
            no_hwaccel=False,
        )

        handle_camera_commands(args)

        mock_client.assert_called_once_with(
            host="192.168.42.1",
            port=6666,
            enable_hwaccel=True,
        )
        mock_client_instance.open.assert_called_once()
        mock_client_instance.start_preview_stream.assert_not_called()
        mock_client_instance.start_capture.assert_called_once()
        mock_client_instance.on_video_stream.assert_not_called()

        mock_logger.info.assert_any_call("Connecting to camera")
        mock_logger.info.assert_any_call("Capturing video")
        mock_logger.info.assert_any_call("Commands executed successfully")
        mock_logger.info.assert_any_call(
            "Press Ctrl+C to stop the commands and close the connection"
        )

    @patch("tools.insta360_cmd.Client")
    @patch("tools.insta360_cmd.logger")
    def test_handle_camera_commands_capture_and_preview(self, mock_logger, mock_client):
        mock_client_instance = MagicMock()
        mock_client.return_value = mock_client_instance

        args = argparse.Namespace(
            command=["capture", "preview"],
            output="./output/dump.mp4",
            display=False,
            no_hwaccel=False,
        )

        handle_camera_commands(args)

        mock_client.assert_called_once_with(
            host="192.168.42.1",
            port=6666,
            enable_hwaccel=True,
        )
        mock_client_instance.open.assert_called_once()
        mock_client_instance.start_preview_stream.assert_called_once()
        mock_client_instance.start_capture.assert_called_once()
        mock_client_instance.on_video_stream.assert_called_once()

        mock_logger.info.assert_any_call("Connecting to camera")
        mock_logger.info.assert_any_call("Starting preview stream")
        mock_logger.info.assert_any_call("Capturing video")
        mock_logger.info.assert_any_call("Commands executed successfully")
        mock_logger.info.assert_any_call(
            "Press Ctrl+C to stop the commands and close the connection"
        )

    @patch("tools.insta360_cmd.Client.open")
    @patch("tools.insta360_cmd.Client.start_preview_stream")
    @patch("tools.insta360_cmd.Client.close")
    @patch("tools.insta360_cmd.logger")
    @patch("tools.insta360_cmd.convert_raw_video_to_output_file")
    def test_handle_camera_commands_preview_output_to_raw(
        self,
        mock_convert_video,
        mock_logger,
        mock_client_close,
        mock_client_start_preview_stream,
        mock_client_open,
    ):
        args = argparse.Namespace(
            command=["preview"],
            output="test.raw",
            display=False,
            no_hwaccel=False,
        )

        handle_camera_commands(args)

        mock_client_open.assert_called_once()
        mock_client_start_preview_stream.assert_called_once()

        mock_logger.info.assert_any_call("Connecting to camera")
        mock_logger.info.assert_any_call("Starting preview stream")
        mock_logger.info.assert_any_call("Commands executed successfully")
        mock_logger.info.assert_any_call(
            "Press Ctrl+C to stop the commands and close the connection"
        )

        mock_convert_video.assert_not_called()

    @patch("tools.insta360_cmd.Client")
    @patch("tools.insta360_cmd.logger")
    def test_handle_camera_commands_preview_output_to_mp4(
        self,
        mock_logger,
        mock_client,
    ):
        mock_client_instance = MagicMock()
        mock_client.return_value = mock_client_instance

        output_path = "test.mp4"

        args = argparse.Namespace(
            command=["preview"],
            output=output_path,
            display=False,
            no_hwaccel=False,
        )

        handle_camera_commands(args)

        mock_client.assert_called_once_with(
            host="192.168.42.1",
            port=6666,
            enable_hwaccel=True,
        )
        mock_client_instance.open.assert_called_once()
        mock_client_instance.start_preview_stream.assert_called_once()
        mock_client_instance.start_capture.assert_not_called()
        mock_client_instance.on_video_stream.assert_called_once_with(wait=True)
        mock_client_instance.on_disconnect.assert_called_once_with(wait=True)

        mock_logger.info.assert_any_call("Connecting to camera")
        mock_logger.info.assert_any_call("Starting preview stream")
        mock_logger.info.assert_any_call("Commands executed successfully")
        mock_logger.info.assert_any_call(
            "Press Ctrl+C to stop the commands and close the connection"
        )

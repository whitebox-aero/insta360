import functools
import time
from unittest import SkipTest


def skip_on_model(
    *,
    model,
    firmware_number=None,
    reason,
    client_attribute_name="camera",
):
    def decorator(test_item):
        @functools.wraps(test_item)
        def test_wrapper(self_, *args, **kwargs):
            client = getattr(self_, client_attribute_name)
            camera_info = client.camera_info

            # First, if not already loaded, give it a bit of time to load
            if not camera_info.loaded:
                time.sleep(0.1)

            # and if we didn't receive the message by then, fail
            if not camera_info.loaded:
                raise ValueError("Camera info not loaded!")

            if camera_info.model == model and (
                not firmware_number or camera_info.firmware_number == firmware_number
            ):
                message = f"by model ({model}"
                if firmware_number:
                    message += f" - {firmware_number}"

                message += f") - {reason}"
                raise SkipTest(message)

            return test_item(self_, *args, **kwargs)

        return test_wrapper

    return decorator

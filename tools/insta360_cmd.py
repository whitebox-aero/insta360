import argparse
import logging
import os

from insta360.rtmp import Client
from insta360.utils.video import (
    check_ffmpeg_installed,
    convert_raw_video_to_output_file,
)

# Setup logging
logger = logging.getLogger("insta360_cmd")
handler = logging.StreamHandler()
formatter = logging.Formatter(
    "%(asctime)s: %(levelname)s: %(name)s: %(message)s", datefmt="%Y-%m-%d %H:%M:%S"
)
handler.setFormatter(formatter)
logger.addHandler(handler)


# region output handling


def _register_output_handler(args, client):
    if not args.output:
        return None, None

    output_fp = args.output

    if output_fp.endswith(".raw"):
        target_fp = output_fp
    else:
        target_fp = output_fp + ".raw"

    data_received = False

    @client.on_video_stream(wait=True)
    async def on_video_stream(content: bytes, **kwargs):
        nonlocal data_received
        data_received = True

        with open(target_fp, "ab") as f:
            f.write(content)

    @client.on_disconnect(wait=True)
    async def on_disconnect(**kwargs):
        nonlocal data_received
        if not data_received:
            logger.info(
                "Aborted before any video data was received, "
                "video file was not saved",
            )
            return

        if not output_fp.endswith(".raw"):
            logger.info("Converting raw video to output file")
            convert_raw_video_to_output_file(
                target_fp,
                output_fp,
                verbose=args.verbose,
            )

            if not args.keep_raw:
                # Do not delete the raw file if the output file was requested
                # to be saved in raw format
                if not output_fp.endswith(".raw"):
                    os.unlink(target_fp)

        logger.info(f"File saved to {output_fp}")

    return target_fp, output_fp


# endregion output handling


def _handle_preview_command(args, client):
    """
    Handler for executing preview command.

    Parameters:
        args: Parsed command line arguments.
        client: Client instance.
    """
    target_fp, output_fp = _register_output_handler(args, client)

    logger.info(f"Starting video preview")

    if output_fp and output_fp.endswith(".raw"):
        logger.info(
            "File will be converted and saved to the provided filepath after "
            f"the capture is complete ({output_fp})"
        )

    client.start_preview_stream()


def handle_camera_commands(args: argparse.Namespace):
    """
    Handler for executing camera commands.

    Parameters:
        args: Parsed command line arguments
    """

    commands = args.command

    logger.info("Connecting to camera")
    client = Client(
        host="192.168.42.1",
        port=6666,
        enable_hwaccel=not args.no_hwaccel,
    )
    client.open()

    command_order = ["preview", "capture"]
    commands = sorted(commands, key=lambda x: command_order.index(x))

    for command in commands:
        if command == "preview":
            logger.info("Starting preview stream")
            _handle_preview_command(args, client)

        elif command == "capture":
            logger.info("Capturing video")
            client.start_capture()

    logger.info("Commands executed successfully")
    logger.info("Press Ctrl+C to stop the commands and close the connection")

    if args.display:
        client.display_stream()


def parse_args():
    """
    Parse command line arguments.

    Returns:
        Parsed arguments.
    """

    parser = argparse.ArgumentParser(
        prog="insta360_cmd",
        description="Control & Interact with Insta360 from command line",
    )

    preview_group = parser.add_argument_group("preview command mode")
    preview_group.add_argument(
        "-d", "--display", action="store_true", help="Display video stream"
    )
    preview_group.add_argument(
        "--no-hwaccel", action="store_true", help="Disable hardware acceleration"
    )
    preview_group.add_argument(
        "-o",
        "--output",
        help=(
            "Save streamed video to a file. Requires running with 'preview' "
            "command. If the desired file extension is not '.raw', ffmpeg must "
            "be installed on the system to convert the raw video to a viewable "
            "format."
        ),
        required=False,
    )
    preview_group.add_argument(
        "-k",
        "--keep-raw",
        action="store_true",
        help=(
            "Keep the raw video file after converting the video. "
            "By default, the raw video file is deleted after converting it to "
            "the output file. Only applicable when --output filepath is not "
            "a '.raw' file."
        ),
    )

    parser.add_argument(
        "-c",
        "--command",
        required=True,
        action="append",
        choices=["preview", "capture"],
        help="Commands to execute",
    )
    parser.add_argument("-v", "--verbose", action="store_true", help="Verbose output")

    args = parser.parse_args()

    if "preview" in args.command:
        if not args.output and not args.display:
            parser.error(
                "When using the 'preview' command, you must specify whether to "
                "display the video stream (-d), save it to a file (-o), or both"
            )

        if (
            args.output
            and not args.output.endswith(".raw")
            and not check_ffmpeg_installed()
        ):
            parser.error(
                "`ffmpeg` is not installed on the system. You can only save "
                "the video in raw format. Please install `ffmpeg` to convert "
                "the raw video to a viewable format."
            )

    return args


def main():
    """
    Entry point for the command line tool.
    """

    args = parse_args()

    if args.verbose:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)

    handle_camera_commands(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.error(e)

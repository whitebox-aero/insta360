import unittest
import random

from datetime import datetime
from unittest.mock import patch, MagicMock
from insta360.osc import *


# Constants
EXTENSIONS = ["jpg", "insp", "insv", "lrv"]


# Functions
def get_file_prefix(extension: str):
    if extension == "jpg":
        return "IMG"
    elif extension == "insp":
        return "IMG"
    elif extension == "insv":
        return "VID"
    elif extension == "lrv":
        return "LRV"
    else:
        return "UNK"


def generate_random_file(extension: str | None = None) -> dict:
    time_now = datetime.now()
    formatted_time = time_now.strftime("%Y%m%d_%H%M%S")
    extension = extension if extension else random.choice(EXTENSIONS)
    file_prefix = get_file_prefix(extension)

    name = f"{file_prefix}_{formatted_time}_01_{random.randint(0, 1000)}.{extension}"
    file_url = f"http://192.168.42.1:80/DCIM/Camera01/{name}"
    local_file_url = f"/DCIM/Camera01/{name}"
    size = random.randint(1000000, 100000000)
    width = 6080
    height = 3040
    dateTimeZone = ""
    _dateTime = time_now.strftime("%Y:%m:%d %H:%M:%S+08:00")
    _thumbnailSize = random.randint(1000, 100000)
    isProcessed = True
    previewUrl = ""
    thumbnail = "iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNk+A8AAQUBAScY42YAAAAASUVORK5CYII="

    return {
        "name": name,
        "fileUrl": file_url,
        "_localFileUrl": local_file_url,
        "size": size,
        "width": width,
        "height": height,
        "dateTimeZone": dateTimeZone,
        "_dateTime": _dateTime,
        "_thumbnailSize": _thumbnailSize,
        "isProcessed": isProcessed,
        "previewUrl": previewUrl,
        "thumbnail": thumbnail,
    }


def list_files_response(
    file_type: ListFileType = ListFileType.ALL,
    start_position: int = 0,
    entry_count: int = 10,
    max_thumb_size: int | None = None,
) -> ListFilesResponse | ErrorResponse:
    random_files = [generate_random_file() for _ in range(entry_count)]

    if max_thumb_size == None:
        for file in random_files:
            file.update({"thumbnail": ""})

    response = {
        "name": "camera.list_files",
        "state": "done",
        "results": {
            "entries": random_files,
            "totalEntries": len(random_files),
        },
    }

    return response


class TestClient(unittest.TestCase):
    def setUp(self):
        self.client = Client()

    @patch("insta360.osc.requests.post")
    def test_execute_command_success(self, mock_post):
        mock_response = MagicMock()
        mock_response.status_code = 200
        mock_response.json.return_value = {
            "name": "camera.delete",
            "results": {"fileUrls": []},
            "state": "done",
        }
        mock_post.return_value = mock_response

        body = {"name": "camera.takePicture"}
        response = self.client.execute_command(body)

        mock_post.assert_called_once_with(
            "http://192.168.42.1/osc/commands/execute", json=body, timeout=10.0
        )
        self.assertEqual(
            response.json(),
            {"name": "camera.delete", "results": {"fileUrls": []}, "state": "done"},
        )

    @patch("insta360.osc.requests.post")
    def test_execute_command_failed(self, mock_post):
        mock_response = MagicMock()
        mock_response.status_code = 400
        mock_response.json.return_value = {
            "error": {
                "code": "invalidParameterValue",
                "message": "Parameter url3 doesn't exist.",
            }
        }
        mock_post.return_value = mock_response

        body = {"name": "camera.takePicture"}
        response = self.client.execute_command(body)

        mock_post.assert_called_once_with(
            "http://192.168.42.1/osc/commands/execute", json=body, timeout=10.0
        )
        self.assertEqual(
            response.json(),
            {
                "error": {
                    "code": "invalidParameterValue",
                    "message": "Parameter url3 doesn't exist.",
                }
            },
        )

    @patch("insta360.osc.requests.post")
    def test_execute_command_exception(self, mock_post):
        mock_post.side_effect = Exception("Error")

        with self.assertRaises(Exception):
            self.client.execute_command({"name": "camera.takePicture"})
        mock_post.assert_called_once()

    @patch("insta360.osc.requests.post")
    def test_list_files(self, mock_post):
        mock_response = MagicMock()
        mock_response.status_code = 200
        mock_response.json.return_value = list_files_response(entry_count=2)
        mock_post.return_value = mock_response

        response = self.client.list_files(entry_count=2)

        mock_post.assert_called_once_with(
            "http://192.168.42.1/osc/commands/execute",
            json={
                "name": "camera.listFiles",
                "parameters": {
                    "fileType": ListFileType.ALL.value,
                    "entryCount": 2,
                    "maxThumbSize": None,
                    "startPosition": 0,
                },
            },
            timeout=10.0,
        )
        self.assertIsInstance(response, ListFilesResponse)

    @patch("insta360.osc.requests.post")
    def test_delete_files(self, mock_post):
        mock_response = MagicMock()
        mock_response.status_code = 200
        mock_response.json.return_value = {
            "name": "camera.delete",
            "results": {"fileUrls": []},
            "state": "done",
        }
        mock_post.return_value = mock_response

        file_urls = [
            "http://192.168.42.1:80/DCIM/Camera01/VID_20240413_051305_00_031.insv",
            "http://192.168.42.1:80/DCIM/Camera01/VID_20240413_051251_00_030.insv",
        ]
        response = self.client.delete_files(file_urls)

        mock_post.assert_called_once_with(
            "http://192.168.42.1/osc/commands/execute",
            json={
                "name": "camera.delete",
                "parameters": {"fileUrls": file_urls},
            },
            timeout=10.0,
        )
        self.assertIsInstance(response, DeleteFilesResponse)

    @patch("insta360.osc.requests.get")
    def test_download_file(self, mock_get):
        mock_response = MagicMock()
        mock_response.status_code = 200
        mock_response.content = b"file content"
        mock_get.return_value = mock_response

        file_url = "http://192.168.42.1:80/DCIM/Camera01/LRV_20240411_074704_11_029.lrv"
        save_path = "/tmp/LRV_20240411_074704_11_029.lrv"
        with patch("builtins.open", unittest.mock.mock_open()) as mock_file:
            result = self.client.download_file(file_url, save_path)

            mock_get.assert_called_once_with(file_url, timeout=10.0)
            mock_file.assert_called_once_with(save_path, "wb")
            mock_file().write.assert_called_once_with(b"file content")
            self.assertTrue(result)

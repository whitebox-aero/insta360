import logging
import time
import unittest
from unittest.mock import Mock, patch

from insta360.rtmp import Client, EventManager
from unit_tests.common import override_logging_level

logger = logging.getLogger(__name__)


class TestEvents(unittest.TestCase):
    def setUp(self) -> None:
        self.camera = Client(logger=logger)
        return super().setUp()

    def tearDown(self) -> None:
        self.camera.close()
        return super().tearDown()

    def test_event_connect(self):
        call_sentinel = Mock()

        @self.camera.on_connect()
        async def handler(**kwargs):
            call_sentinel(**kwargs)

        self.camera.open()
        time.sleep(0.5)
        call_sentinel.assert_called_with(client=self.camera)

    def test_event_disconnect(self):
        call_sentinel = Mock()

        @self.camera.on_disconnect()
        async def handler(**kwargs):
            call_sentinel(**kwargs)

        self.camera.open()
        call_sentinel.assert_not_called()

        time.sleep(2)
        self.camera.close()
        time.sleep(0.5)
        call_sentinel.assert_called_with(client=self.camera)

    def test_event_video_stream(self):
        self.camera.open()
        call_sentinel = Mock()

        # Register the event to receive everything the camera streams to us
        @self.camera.on_video_stream(wait=True)
        async def handler(content, **kwargs):
            # Write everything that comes through so that it can be compared to
            # the file that is written by the internal mechanism
            call_sentinel(content)

        # Start a preview stream that will send over some data for processing
        seq = self.camera.start_preview_stream()
        self.camera._check_if_command_successful(seq)
        time.sleep(2)
        seq = self.camera.stop_preview_stream()
        self.camera._check_if_command_successful(seq)

        # Check whether there were calls. Stream usually sends quite a few
        # batches of video data
        assert call_sentinel.call_count > 10

        # and ensure that all of them were byte data
        for call in call_sentinel.call_args_list:
            assert isinstance(call.args[0], bytes)

    def test_event_error_socket_connect(self):
        call_sentinel = Mock()
        raised_exception = RuntimeError("It no work!")

        @self.camera.on_error()
        async def handler(**kwargs):
            call_sentinel(**kwargs)

        with (
            patch.object(
                self.camera,
                "_open_camera_socket",
                side_effect=raised_exception,
            ),
            override_logging_level(logger, logging.CRITICAL),
        ):
            is_connected = self.camera.open()

        assert not is_connected

        time.sleep(0.1)
        call_sentinel.assert_called_with(
            client=self.camera,
            exception=raised_exception,
            message="Failed to open socket",
        )


class TestEventManager(unittest.TestCase):
    def setUp(self):
        self.manager = EventManager()
        return super().setUp()

    def test_event_registered_unregistered(self):
        call_sentinel = Mock()
        uid = "some-letters-here"
        event_name = "connect"

        async def handler(**kwargs):
            call_sentinel(**kwargs)

        self.manager.register_handler(event_name, handler, uid)
        self.manager.unregister_handler(
            event_name=event_name,
            uid=uid,
        )

        self.manager.process_event(event_name, test=1)
        time.sleep(0.5)

        call_sentinel.assert_not_called()

    @patch("threading.Thread.start")
    def test_event_thread_started_only_once(self, mock_thread_start):
        # GIVEN there are some events and handlers (more than 1)
        event_name_1 = "any_event"

        async def any_handler(**kwargs):
            pass

        event_name_2 = "any_other_event"

        async def any_other_handler(**kwargs):
            pass

        # WHEN they are registered to the event manager
        self.manager.register_handler(event_name_1, any_handler)
        self.manager.register_handler(event_name_2, any_other_handler)

        # THEN both events should be registered, but the event loop thread
        #      should have only been started exactly once
        mock_thread_start.assert_called_once()
        assert len(self.manager._registry[event_name_1]) == 1
        assert len(self.manager._registry[event_name_2]) == 1

    def test_event_handler_malformed(self):
        event_name = "good_event"

        async def bad_handler(does_not_have_all_args):
            pass

        self.manager.register_handler(event_name, bad_handler)

        with patch.object(self.manager.logger, "exception") as le:
            self.manager.process_event(event_name, test=1)

        le.assert_called_once()

    def test_waited_event_handler_failing_not_raising_exception(self):
        event_name = "failing_event"

        async def failing_handler(**kwargs):
            raise Exception("Execute order 66")

        self.manager.register_handler(event_name, failing_handler, wait=True)

        with patch.object(self.manager.logger, "exception") as logger_exc:
            self.manager.process_event(event_name)

        logger_exc.assert_called_once_with(
            "Error occurred while waiting for handler to finish "
            '("{}") for event "{}"'.format(
                failing_handler.__name__,
                event_name,
            ),
        )

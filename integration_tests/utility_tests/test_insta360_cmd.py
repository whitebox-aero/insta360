import os
import unittest
import tempfile
import time
import subprocess
import mimetypes


class TestInsta360CMD(unittest.TestCase):
    def setUp(self) -> None:
        self.tmp_dir = tempfile.TemporaryDirectory()
        return super().setUp()

    def tearDown(self) -> None:
        self.tmp_dir.cleanup()
        return super().tearDown()

    def test_invalid_command(
        self,
    ):
        cmd = ["python3", "./insta", "-c", "invalid"]
        process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        time.sleep(10)
        process.terminate()
        time.sleep(10)

        self.assertEqual(process.returncode, 2)

    def test_capture(
        self,
    ):
        cmd = ["python3", "./insta", "-c", "capture"]
        process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        time.sleep(10)
        process.terminate()
        time.sleep(10)

    def test_preview_with_output(self):
        output_file_mp4 = os.path.join(self.tmp_dir.name, "output.mp4")
        output_file_raw = output_file_mp4 + ".raw"

        cmd = [
            "python3",
            "./insta",
            "-c",
            "preview",
            "-o",
            output_file_mp4,
        ]
        process = subprocess.Popen(
            cmd,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )

        time.sleep(10)
        process.terminate()
        time.sleep(10)

        raw_file_exists = os.path.exists(output_file_raw)
        self.assertFalse(raw_file_exists)

        mp4_file_exists = os.path.exists(output_file_mp4)
        self.assertTrue(mp4_file_exists)

        file_type = mimetypes.guess_type(output_file_mp4)[0]
        self.assertEqual(file_type, "video/mp4")

    def test_preview_with_output_keep_raw(self):
        output_file_mp4 = os.path.join(self.tmp_dir.name, "output.mp4")
        output_file_raw = output_file_mp4 + ".raw"

        cmd = [
            "python3",
            "./insta",
            "-c",
            "preview",
            "-o",
            output_file_mp4,
            "-k",  # keep RAW file
        ]
        process = subprocess.Popen(
            cmd,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )

        time.sleep(10)
        process.terminate()
        time.sleep(10)

        raw_file_exists = os.path.exists(output_file_raw)
        self.assertTrue(raw_file_exists)

        mp4_file_exists = os.path.exists(output_file_mp4)
        self.assertTrue(mp4_file_exists)

        file_type_raw = mimetypes.guess_type(output_file_raw)[0]
        # RAW file is just dumped bytes straight from the stream, it could
        # return any mime type, so let's only check if it's not MP4 as below
        self.assertNotEqual(file_type_raw, "video/mp4")

        file_type_mp4 = mimetypes.guess_type(output_file_mp4)[0]
        self.assertEqual(file_type_mp4, "video/mp4")

    def test_capture_and_preview_stream(self):
        output_file_mp4 = os.path.join(self.tmp_dir.name, "output.mp4")
        output_file_raw = output_file_mp4 + ".raw"

        cmd = [
            "python3",
            "./insta",
            "-c",
            "capture",
            "-c",
            "preview",
            "-o",
            output_file_mp4,
        ]
        process = subprocess.Popen(
            cmd,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )

        time.sleep(10)
        process.terminate()
        time.sleep(10)

        raw_file_exists = os.path.exists(output_file_raw)
        self.assertFalse(raw_file_exists)

        mp4_file_exists = os.path.exists(output_file_mp4)
        self.assertTrue(mp4_file_exists)

        file_type = mimetypes.guess_type(output_file_mp4)[0]
        self.assertEqual(file_type, "video/mp4")

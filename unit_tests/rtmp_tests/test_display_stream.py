import unittest
import threading
import time

from unittest.mock import MagicMock, patch
from unit_tests.common import ensure_camera_connected
from insta360.rtmp import Client


class TestDisplayStream(unittest.TestCase):
    @patch(
        "insta360.rtmp.rtmp.ensure_camera_connected",
        side_effect=ensure_camera_connected,
    )
    def setUp(self, _mock_ensure_camera_connected) -> None:
        self.camera = Client()
        return super().setUp()

    def tearDown(self) -> None:
        self.camera.close()
        return super().tearDown()

    @patch("insta360.rtmp.rtmp.select.select")
    def test_process_frame(self, mock_select):
        self.camera.show_stream = True
        self.camera.stdout_timeout = 1
        self.camera.show_frame_size = 10
        self.camera.show_frame_height = 2
        self.camera.show_frame_width = 5

        self.camera.ffmpeg_proc = MagicMock()
        self.camera.ffmpeg_proc.stdout.read.return_value = b"\x00" * 10

        def side_effect(*args, **kwargs):
            self.camera.show_stream = False
            return ([self.camera.ffmpeg_proc.stdout], [], [])

        mock_select.side_effect = side_effect

        self.camera._process_frame()
        self.assertEqual(self.camera.ffmpeg_proc.stdout.read.call_count, 1)

    @patch("insta360.rtmp.rtmp.cv2.imshow")
    @patch("insta360.rtmp.rtmp.cv2.waitKey", return_value=1)
    @patch("insta360.rtmp.rtmp.subprocess.Popen")
    @patch("insta360.rtmp.Client._process_frame")
    def test_display_stream(
        self, mock_process_frame, mock_popen, mock_waitKey, mock_imshow
    ):
        mock_ffmpeg_proc = MagicMock()
        mock_ffmpeg_proc.stdout = MagicMock()
        mock_ffmpeg_proc.stdin = MagicMock()
        mock_ffmpeg_proc.stderr = MagicMock()
        mock_popen.return_value = mock_ffmpeg_proc

        self.camera.frame_queue = MagicMock()
        self.camera.frame_queue.get.side_effect = [
            MagicMock(shape=(2, 5, 3)) for _ in range(3)
        ]

        display_thread = threading.Thread(target=self.camera.display_stream)
        display_thread.start()

        time.sleep(1)

        self.camera.show_stream = False
        display_thread.join()

        self.assertTrue(mock_popen.called)
        self.assertTrue(mock_process_frame.called)
        self.assertEqual(mock_imshow.call_count, 3)
        self.assertEqual(mock_waitKey.call_count, 3)

import unittest
import logging
import threading
import time

from insta360.rtmp import Client
from unittest.mock import patch, MagicMock


logging.basicConfig(level=logging.ERROR)
logger = logging.getLogger(__name__)


class TestDisplayStream(unittest.TestCase):
    def setUp(self) -> None:
        self.camera = Client(logger=logger)
        self.camera.logger.error = MagicMock()
        self.camera.open()
        return super().setUp()

    def tearDown(self) -> None:
        self.camera.close()
        return super().tearDown()

    def test_display_stream(self):
        def switch_show_stream():
            time.sleep(5)
            self.camera.show_stream = False

        switch_thread = threading.Thread(target=switch_show_stream)
        switch_thread.start()

        self.camera.start_preview_stream()
        self.camera.display_stream()
        self.assertFalse(self.camera.show_stream)

## Setting up events

RTMP client provides several events you can register your handlers for:
- `connect`
- `disconnect`
- `error`
- `video_stream`


## Registering events

To register an event, use the decorator method from the RTMP `Client` instance:

```py
from insta360.rtmp import Client

client = Client()

@client.on_event('video_stream')
async def on_video_stream_handler(content, **kwargs):
    byte_count = len(content)
    print(f"Look, we just got {byte_count} bytes of video material!")
```

You can also use helper methods available, named `on_EVENT_NAME`,
e.g. `client.on_connect()` instead of `client.on_event('connect')`. 

All the handlers need to either use `**kwargs`, or specify all the provided
data in the arguments. For more info refer to the specific events below.

### Event processing order

Event handlers are processed asynchronously, as the events come by.

In case you need to ensure events get processed sequentially, you can use the
`wait=True` argument when registering an event.

```py
@client.on_event('disconnect', wait=True)
async def handler(content, **kwargs):
    # Perform some tasks upon disconnect
```

When using `wait=True`, you have to ensure that the handler executes as fast as
possible, as it could cause the camera connection to be dropped if blocking for
too long.

For example, if you need to process the received video frames in sequential
order to ensure that the previous one is processed before the next, you can use
a queue to store the frames so that they could be processed separately.

```py
from threading import Lock

lock = Lock()
buffer = []

@client.on_event('video_stream', wait=True)
async def handler(content, **kwargs):
    with lock:
        buffer.append(content)
```

and then, in a thread that is processing the data, use something like:

```python
while True:
    with lock:
        if not buffer:
            continue

        next_frame = buffer.pop(0)

    # run some code here to process the frame
    # without keeping the lock on the buffer
```

Depending on your needs, you might also want to use something like a
[Queue](https://docs.python.org/3/library/queue.html).

## Available events

### `connect` event

Triggered upon successful connection.

Provided data:
- `client` (`insta360.rtmp.Client`): object where the event originated from


### `disconnect` event

Triggered upon disconnection.

Provided data:
- `client` (`insta360.rtmp.Client`): object where the event originated from


### `error` event

Triggered upon an error with connectivity with the camera.

Provided data:
- `client` (`insta360.rtmp.Client`): object where the event originated from


### `video_stream` event

Triggered upon receiving video stream data from the camera.

Provided data:
- `client` (`insta360.rtmp.Client`): object where the event originated from
- `content` (`bytes`): raw video packets that were received from the camera

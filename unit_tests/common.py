import hashlib
import logging
from contextlib import contextmanager


# Mock Classes
class MockResponse:
    def __init__(self, body: dict, status_code: int):
        self.body = body
        self.status_code = status_code

    def json(self):
        return self.body


def ensure_camera_connected(connect_host: str):
    return MockResponse({}, 200)


def get_file_sha1(fd):
    hash_ = hashlib.sha1()
    buffer_size = 65536  # read stuff in 64kb chunks

    while True:
        data = fd.read(buffer_size)
        if not data:
            break

        hash_.update(data)

    return hash_.hexdigest()


@contextmanager
def override_logging_level(
    logger: str | logging.Logger,
    target_level,
):
    if isinstance(logger, str):
        logger = logging.getLogger(logger)

    previous_level = logger.level
    logger.setLevel(target_level)

    try:
        yield
    finally:
        logger.setLevel(previous_level)
